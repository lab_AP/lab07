#ifndef STACK_PUBLIC_H
#define STACK_PUBLIC_H
#include "stack_private.h"

struct e *push (struct e *, int);
struct e *pop (struct e *, int *, int *);
void traversal (struct e *);


#endif
