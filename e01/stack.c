#include "stack_private.h"


struct e *
push (
  struct e *pTop,
  int val
  )
{
  struct e *pNew;

  pNew = newE ();

  pNew->key = val;
  pNew->next = pTop;
  pTop = pNew;

  return (pTop);
}

struct e *
pop (
  struct e *pTop,
  int *val,
  int *status
  )
{
  struct e *pOld;

  if (pTop != NULL) {
    *status = SUCCESS;
    *val = pTop->key;
    pOld = pTop;
    pTop = pTop->next;
    free (pOld);
  } else {
    *status = FAILURE;
  }

  return (pTop);
}

void
traversal (
  struct e *pTop
  )
{
  struct e *pTmp;

  fprintf (stdout, "pTop -> ");
  pTmp = pTop;
  while (pTmp != NULL) {
    fprintf (stdout, "%d -> ", pTmp->key);
    pTmp = pTmp->next;
  }

  fprintf (stdout, "NULL \n");

  return;
}


void
read (
  int *val
  )
{
  fprintf (stdout, "Element: ");
  scanf ("%d", val);

  return;
}

void
write (
  int val,
  int status
  )
{
  if (status == SUCCESS) {
    fprintf (stdout, "Element: %d\n", val);
  } else {
    fprintf (stdout, "Empty stack.\n");
  }

  return;
}



