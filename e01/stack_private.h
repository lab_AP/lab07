#ifndef STACK_PRIVATE_H
#define STACK_PRIVATE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR    10

#define SUCCESS 1
#define FAILURE 0

struct e {
  int key;
  /* ... other data fields ... */
  struct e *next;
  };

struct e *newE ();
void read (int *);
void write (int, int);

#endif
