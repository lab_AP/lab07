#include "stack_private.h"


struct e *
newE (
  )
{
  struct e *ePtr;

  ePtr = (struct e *) malloc (sizeof (struct e));

  if (ePtr==NULL) {
    fprintf (stderr, "Allocation error.\n");
    exit (FAILURE);
  }

  return (ePtr);
}
