#ifndef STACK_PUBLIC_H
#define STACK_PUBLIC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR    10

#define SUCCESS 1
#define FAILURE 0

struct e {
  int key;
  /* ... other data fields ... */
  struct e *next;
  };

struct e *push (struct e *, int);
struct e *pop (struct e *, int *, int *);
struct e *newE ();
void traversal (struct e *);
void read (int *);
void write (int, int);

#endif
