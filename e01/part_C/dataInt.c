#include "dataInt.h"

void
traversal (
  struct e *pTop
  )
{
  struct e *pTmp;

  fprintf (stdout, "pTop -> ");
  pTmp = pTop;
  while (pTmp != NULL) {
    fprintf (stdout, "%d -> ", pTmp->item);
    pTmp = pTmp->next;
  }

  fprintf (stdout, "NULL \n");

  return;
}


void
read (
  int *val
  )
{
  fprintf (stdout, "Element: ");
  scanf ("%d", val);

  return;
}
