#include "dataString.h"

void
traversal (
  struct e *pTop
  )
{
  struct e *pTmp;

  fprintf (stdout, "pTop -> ");
  pTmp = pTop;
  while (pTmp != NULL) {
    fprintf (stdout, "%s -> ", pTmp->item);
    pTmp = pTmp->next;
  }

  fprintf (stdout, "NULL \n");

  return;
}


void
read (
  item *val
  )
{
  fprintf (stdout, "Element: ");
  scanf ("%s", val);

  return;
}
