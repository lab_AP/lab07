#include "stack.h"
#include "dataInt String.h"


struct e *
newE (
  )
{
  struct e *ePtr;

  ePtr = (struct e *) malloc (sizeof (struct e));

  if (ePtr==NULL) {
    fprintf (stderr, "Allocation error.\n");
    exit (FAILURE);
  }
  return (ePtr);
}

struct e *
push (
  struct *pTop,
  item val
  )
{
  struct e *pNew;

  pNew = newE ();

  pNew->key = val;
  pNew->next = pTop;
  pTop = pNew;

  return (pTop);
}

struct e *
pop (
  struct e *pTop,
  int *val,
  int *status
  )
{
  struct e *pOld;

  if (pTop != NULL) {
    *status = SUCCESS;
    *val = pTop->key;
    pOld = pTop;
    pTop = pTop->next;
    free (pOld);
  } else {
    *status = FAILURE;
  }

  return (pTop);
}

void
write (
  int val,
  int status
  )
{
  if (status == SUCCESS) {
    fprintf (stdout, "Element: %d\n", val);
  } else {
    fprintf (stdout, "Empty stack.\n");
  }

  return;
}



