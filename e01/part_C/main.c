#include "stack_public.h"

int
main (
  void
  )
{
  char scelta[MAX_STR];
  struct e *pTop;
  int val, status;

  pTop = NULL;

  do {
    printf (
      "User choice (push (p), pop (o), traveral (t), quit (end)): ");
    scanf ("%s", scelta);

    if (strcmp (scelta, "p") == 0) {
      read (&val);
      pTop = push (pTop, val);
    } else
      if (strcmp (scelta, "o") == 0) {
	pTop = pop (pTop, &val, &status);
	write (val, status);
      } else {
	if (strcmp (scelta, "t") == 0) {
	  traversal (pTop);
	} else {
	  if (strcmp (scelta, "end") != 0) {
	    printf ("Wrong Choice.\n");
	  }
	}
      }
  } while (strcmp (scelta, "end") != 0);

  return (SUCCESS);
}
