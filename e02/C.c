#include <stdio.h>

int catalan (int n);

int main(void)
{
  int i;

  for (i=0; i<10; i++)
  {
    printf("\nC(%d) = %d", i, catalan(i));
  }
  return 0;
}

int catalan (int n)
{
  int i;
  int res = 0;
  if (n==0 || n==1)
    return(1);
  else{
    for (i=0; i<n; i++)
      res = res + catalan(i)*catalan(n-1-i);
    return res;
  }
}
