#include <stdio.h>


void triangle(int n);

int main(void)
{
  triangle(20);
  return 0;
}

void triangle(int n)
{
  int i;

  if(n==0){
    return;
  }

  triangle(n-1);
  for (i=0; i<n; i++)
    printf("%d ", n);
  printf("\n");
  return;

}
