#include <stdio.h>

int mult(int, int);

int main(
    void
    )
{
  int  tmp;
  int i1 = 101;
  int i2 = 9;
  if (i1>i2){
    tmp = i2;
    i2 = i1;
    i1 = tmp;
  }
  printf("%d * %d = %d\n", i1, i2, mult(i1,i2));
  return 0;
}

int mult(int i1, int i2)
{
  if (i1==1)
    return i2;

  return(i2 + mult(i1-1, i2));
}

