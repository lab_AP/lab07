#include <stdio.h>
#include <string.h>
int isPalindrome (char *s, int l);


int main(){
  char str[] = "abjhfjqGkayakkayakba";
  int l;
  int itis;
  l = strlen(str);
  
  itis = isPalindrome(str, l);

  printf("The string %s a palindrome.\n", (itis == 1)?"IS":"IS NOT");

  return 0;
}

int isPalindrome(char *s, int l)
{
  if (l <= 1)
    return 1;
  if (s[0] == s[l-1] && isPalindrome(s+sizeof(char), l-2))
    return 1;
  else
    return 0;
}
