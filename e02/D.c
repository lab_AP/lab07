#include <stdio.h>
#include <ctype.h>

int countSpaces(char *s);

int main()
{
  char str[] = "This is string with spaces!";
  printf("\nThe string has %d white spaces !\n", countSpaces(str));

  return 0;
}


int countSpaces(char *s)
{
  if (s[0] == '\0')
    return 0;
  else if (isspace(s[0])){
    return 1+countSpaces(s+sizeof(char));
  }
  return countSpaces(s+sizeof(char));
}
